# Assemblyline archive cluster installation
These are the steps to install an Assemblyline archive cluster. An archive cluster is a extra cluster to add to your infrastructure as read-only to keep historical data of your submissions. Since this is a supplemental cluster it assumes that you already have a [cluster](install_cluster.md) installed.

For more information, read the [reference manual](https://bitbucket.org/cse-assemblyline/assemblyline/src/master/manuals/).

## Installation
Unless otherwise noted perform these steps in the order listed.

## Pre-requisites

* All boxes have a fresh ubuntu 16.04.x server install. (See [Install notes](install_ubuntu_server.md))
* You have at minimum 6 servers: 1 core, 5 riak
  * Core server needs at least 16 CPU threads, 96GB ram and 1TB storage
  * Riak nodes need at least 16 CPU threads, 96GB ram and 1TB storage
* You are on a network connected to the internet and can download file from Amazon S3

## On All Boxes
We recommend using clusterssh when performing operations on multiple computers to ensure they are all configured the same way.

### Setup Environment

    sudo apt-get update
    sudo apt-get -y install git ssh python

    cat >> ~/.bashrc <<EOF
    export PYTHONPATH=/opt/al/pkg
    source /etc/default/al
    EOF

    source ~/.bashrc

    sudo mkdir -p ${PYTHONPATH} &&
    sudo chown -R `whoami`:`groups | awk '{print $1}'` ${PYTHONPATH}/.. &&
    cd ${PYTHONPATH}

## CORE Server Pre-Install

### Clone/create main repos

    cd $PYTHONPATH
    git clone https://bitbucket.org/cse-assemblyline/assemblyline.git -b prod_3.2
    
### Create cluster deployment
**IMPORTANT**
While running the following command, you will be asked a series of questions concerning your infrastructure.
You will need to know the following things before you run the script:

* Name of your deployment
* Acronym for your organization
* Password you want to give the admin user
* Fully qualified domain name for your web interface
* Core server IP
* IPs and amount of RAM on all of the riak nodes
* IP of your current log server if you have one

Once you have all that info you can create your deployment.

    /opt/al/pkg/assemblyline/deployment/create_deployment.py

    # Answer the questions from deployment script
    # NOTE:
    #    Answer to "Which deployment type would you like?" has to be: 4
    #    Answer to "Where would you like us to create your deployment?" has to be: /opt/al/pkg

### Initialise a repo for your al_private

    cd ${PYTHONPATH}/al_private
    git init
    git add -A
    git config user.email "core@al.private"
    git config user.name "Core server"
    git commit -a -m "Initial commit for al_private"

**NOTE**: You can use a real email and user for your private repo and add a remote to push it to your git

### Clone all other repos

    ./assemblyline/al/run/setup_dev_environment.py al_private.seeds.deployment.seed

### Create temporary git server

    mkdir ${PYTHONPATH}/../git && cd ${PYTHONPATH}/../git
    git clone --bare ../pkg/assemblyline/ assemblyline && (cd assemblyline && git update-server-info)
    git clone --bare ../pkg/al_ui/ al_ui && (cd al_ui && git update-server-info)
    git clone --bare ../pkg/al_private/ al_private && (cd al_private && git update-server-info)
    cd ${PYTHONPATH}/.. && sudo python -m SimpleHTTPServer 80

    # Note: Leave web server running in a window (you will return to this window later).

## Riak Nodes (using cluster SSH)

### Set AL_SEED to an appropriate value and update .bashrc
**NOTE**: Set AL_CORE_IP to the IP of your CORE node

    cat >> ~/.bashrc <<EOF
    export AL_SEED=al_private.seeds.deployment.seed
    export AL_CORE_IP=
    EOF

    source ~/.bashrc

### Clone assemblyline repo

    cd $PYTHONPATH
    git clone http://${AL_CORE_IP}/git/al_private
    git clone http://${AL_CORE_IP}/git/assemblyline

### Run install script

    /opt/al/pkg/assemblyline/al/install/install_riak.py
    sudo reboot

    # Login and run the script again.
    /opt/al/pkg/assemblyline/al/install/install_riak.py

## Returning to CORE Server

### Stop temporary git HTTP server
    # Switch back to the window containing the running SimpleHTTPServer.
    ^C
    rm -rf git

### Run install script with install seed

    export AL_SEED=al_private.seeds.deployment.seed
    /opt/al/pkg/assemblyline/al/install/install_core.py
